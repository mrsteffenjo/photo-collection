package net.zeleon.photocol.eventstore;

import net.zeleon.photocol.event.Event;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class InMemoryEventStream implements EventStream {
    private final long version;
    private final List<Event> events;

    public InMemoryEventStream() {
        this.version = 0;
        events = Collections.emptyList();
    }

    public InMemoryEventStream(long version, List<Event> events) {
        this.version = version;
        this.events = events;
    }

    public InMemoryEventStream append(List<? extends Event> newEvents) {
        List<Event> events = new LinkedList<>(this.events);
        events.addAll(newEvents);
        return new InMemoryEventStream(version + 1, Collections.unmodifiableList(events));
    }

    public Iterator<Event> iterator() {
        return events.iterator();
    }

    public long version() {
        return version;
    }
}
