package net.zeleon.photocol.eventstore;

import net.zeleon.photocol.event.Event;

public interface EventStream extends Iterable<Event> {
    long version();
}
