package net.zeleon.photocol.eventstore;

import net.zeleon.photocol.event.Event;

import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class InMemoryEventStore implements EventStore {
    private final Map<UUID, InMemoryEventStream> streams = new ConcurrentHashMap<>();

    public InMemoryEventStream loadEventStream(UUID aggregateId) {
        return streams.computeIfAbsent(aggregateId, k -> new InMemoryEventStream());
    }

    public void store(UUID aggregateId, long version, List<? extends Event> events) {
        InMemoryEventStream stream = loadEventStream(aggregateId);
        if (stream.version() != version) {
            throw new ConcurrentModificationException("Stream has already been modified");
        }
        streams.put(aggregateId, stream.append(events));
    }
}
