package net.zeleon.photocol.eventstore;

import net.zeleon.photocol.event.Event;

import java.util.List;
import java.util.UUID;

public interface EventStore {
    EventStream loadEventStream(UUID aggregateId);

    void store(UUID aggregateId, long version, List<? extends Event> events);
}
