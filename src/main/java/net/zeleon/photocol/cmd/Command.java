package net.zeleon.photocol.cmd;

import java.util.UUID;

public interface Command {
    UUID getAggregateId();
}
