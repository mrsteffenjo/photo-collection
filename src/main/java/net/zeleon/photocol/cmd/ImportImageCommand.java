package net.zeleon.photocol.cmd;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.util.UUID;

@Value
@Builder
@AllArgsConstructor(onConstructor = @__({@JsonCreator}))
public class ImportImageCommand implements Command {
    @NonNull public final UUID aggregateId;
    @NonNull public final UUID importedByUser;
    @NonNull public final UUID importedToWorkspace;
    @NonNull public final String file;
}
