package net.zeleon.photocol;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.zeleon.photocol.aggregate.ImageAggregate;
import net.zeleon.photocol.cmd.ImportImageCommand;
import net.zeleon.photocol.event.Event;
import net.zeleon.photocol.event.ImageImportedEvent;
import net.zeleon.photocol.eventstore.InMemoryEventStore;
import net.zeleon.photocol.eventstore.InMemoryEventStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

public class App {
    private static final Logger log = LoggerFactory.getLogger(App.class);
    private static final ObjectMapper mapper = new ObjectMapper();

	private App() {
	}

	public static void main(String[] args) throws Exception {
        InMemoryEventStore eventStore = new InMemoryEventStore();
        ApplicationService service = new ApplicationService(eventStore, ImageAggregate.class);

        UUID imageArrgregateId = UUID.randomUUID();
        UUID userAggregateId = UUID.randomUUID();
        UUID workspaceAggregateId = UUID.randomUUID();

        service.handle(ImportImageCommand.builder()
                .aggregateId(imageArrgregateId)
                .importedByUser(userAggregateId)
                .importedToWorkspace(workspaceAggregateId)
                .file("/path/file.ext")
                .build());

        ImageAggregate aggregate = new ImageAggregate();
        InMemoryEventStream events = eventStore.loadEventStream(imageArrgregateId);
        for (Event event : events) {
            if (event instanceof ImageImportedEvent) {
                ImageImportedEvent specificEvent = (ImageImportedEvent) event;
                log.info("Created an event object with id {}: {}", specificEvent.aggregateId, event);
                log.info(mapper.writeValueAsString(specificEvent));
                aggregate.handle(specificEvent);
            }
        }

        log.info(aggregate.toString());
    }
}
