package net.zeleon.photocol;

public class Sneak {
    public static RuntimeException sneakyThrow(Throwable t) {
        if (t == null) throw new NullPointerException("t");
        Sneak.sneakyThrow0(t);
        return null;
    }

    @SuppressWarnings("unchecked")
    private static <T extends Throwable> void sneakyThrow0(Throwable t) throws T {
        Throwable s = (t != null) ? ((T) t) : new RuntimeException("No exception to sneak");
        try {
            throw s;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}
