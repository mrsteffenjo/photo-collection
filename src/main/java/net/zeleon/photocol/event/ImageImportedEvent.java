package net.zeleon.photocol.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.*;

import java.util.UUID;

@Value
@Builder
@AllArgsConstructor(onConstructor = @__({@JsonCreator}))
public class ImageImportedEvent implements Event {
    @NonNull public final UUID aggregateId;
    @NonNull public final UUID importedByUser;
    @NonNull public final UUID importedToWorkspace;
    @NonNull public final String file;
}
