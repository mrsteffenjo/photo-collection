package net.zeleon.photocol.aggregate;

import net.zeleon.photocol.cmd.Command;
import net.zeleon.photocol.event.Event;

import java.util.List;

public interface Aggregate {
    List<? extends Event> handle(Command command);

    void handle(Event event);
}
