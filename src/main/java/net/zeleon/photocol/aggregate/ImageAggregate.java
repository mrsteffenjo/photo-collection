package net.zeleon.photocol.aggregate;

import net.zeleon.photocol.cmd.ImportImageCommand;
import net.zeleon.photocol.event.Event;
import net.zeleon.photocol.event.ImageImportedEvent;

import java.io.File;
import java.util.Collections;
import java.util.List;

public class ImageAggregate {
    String id;
    String filename;
    String extension;
    String path;

    public List<? extends Event> handle(ImportImageCommand command) {
        return Collections.singletonList(ImageImportedEvent.builder()
                .aggregateId(command.aggregateId)
                .importedByUser(command.importedByUser)
                .importedToWorkspace(command.importedToWorkspace)
                .file(command.file)
                .build());
    }

    public void handle(ImageImportedEvent event) {
        this.id = event.aggregateId.toString();
        File file = new File(event.file);
        this.filename = file.getName();
        this.extension = filename.substring(filename.lastIndexOf('.')+1);
        this.path = file.getPath();
    }

    @Override
    public String toString() {
        return "ImageAggregate{" +
                "id='" + id + '\'' +
                ", filename='" + filename + '\'' +
                ", extension='" + extension + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}
