# Description
I want to experiment with Event Sourcing and I wanted to make a photo collection utility
 to manage all my photos with tracking of copies and alterations. Not necessarily a good 
 combination, but one I'm going to make due to "hobby".

# Requirements
## A smart IDE
* Lombok plugin
* Additional Java compiler argument `-parameters`
* Annotation processing activated

## Basic
* Maven

# Immutable & Serializable objects
Example object:
```
@Value
@Builder
@AllArgsConstructor(onConstructor = @__({@JsonCreator}))
public class ImageImportedEvent implements Event {
    public final UUID aggregateId;
    public final String file;
}
```
This will give me an object that can be serialized using jackson, 
 created with a builder and is immutable. All at the cost of minimal code and without
 the need to use `getXxx()` every time I want to read the values. 
 As a bonus I also get an `toString()` method that will format the object nicely. 

Example object creation and usage:
```
ImageImportedEvent event = ImageImportedEvent.builder()
                .aggregateId(UUID.randomUUID())
                .file("/path/file.ext")
                .build();
log.info("Created an event object with id {}: {}", event.aggregateId, event);
```
Example utilizing `toString()` of log output:
```
Created an event object with id 35ef18b2-1db3-401a-b5c9-168dcf3eac9c: ImageImportedEvent(aggregateId=35ef18b2-1db3-401a-b5c9-168dcf3eac9c, file=/path/file.ext)
```
Example serialized output:
```
{"aggregateId":"35ef18b2-1db3-401a-b5c9-168dcf3eac9c","file":"/path/file.ext"}
```